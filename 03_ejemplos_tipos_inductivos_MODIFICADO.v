
(* ###################################################################### *)
(** * Tipos enumerados. E.g. Dias de la semana *)

(** La siguiente define un nuevo tipo. El tipo se llama [day] y sus siete
    miembros son [monday], [tuesday], etc. Cada línea de la declaración
    significa: [monday] es un [day], [tuesday] es un [day], etc. *)
Inductive day : Type :=
  | monday : day
  | tuesday : day
  | wednesday : day
  | thursday : day
  | friday : day
  | saturday : day
  | sunday : day.


(** Una vez definido el tipo [day], podemos escribir funciones que operen
    sobre el tipo. *)
Definition next_weekday (d:day) : day :=
  match d with
  | monday => tuesday
  | tuesday => wednesday
  | wednesday => thursday
  | thursday => friday
  | friday => monday
  | saturday => monday
  | sunday => monday
  end.


(** Una vez declarada la función, vamos a comprobar que existe con unos ejemplos. 
    En la siguiente declaración, afirmamos que el siguiente día laborable después
    del sábado es el lunes. *)
(** La táctica [simpl] evalúa (o simplifica) la expresión de acuerdo a la(s) 
    definicion(es) que aparecen en ella. En este caso [next_weekday] *)
Example test_next_weekday:
  next_weekday saturday = monday.
Proof. 
(*Aplica las definiciones*)
  simpl.
  trivial.  
Qed.




(* ###################################################################### *)
(** ** Números *)


(** Recordemos que los naturales son también un tipo inductivo, pues COQ los define 
    a partir de "reglas inductivas": 

Inductive nat : Type :=
  | O : nat
  | S : nat -> nat.

   La definición implica lo siguiente: 
      - [O] es un número natural (letra [O], no número [0]).
      - [S] es un "constructor" que toma como entrada un natural y devuelve el siguiente.
        Es decir, si [n] es un natural, entonces [S n] también lo es. 

   Ahora que está el tipo definido, podemos definir funciones sobre él. *)

(** Definimos la función doble de forma recursiva mediante [Fixpoint] *)
Fixpoint doble (n: nat): nat :=
    match n with
    | O => O
    | S p => S ( S (doble p) )
    end.

Eval compute in doble O. (* doble 0 = 0 *)
Eval compute in doble (S(S(S(S O)))). (* doble 4 = 8 *)

(** Definimos la función suma de forma recursiva mediante [Fixpoint] *)
Fixpoint plus (n : nat) (m : nat) : nat :=
  match n with
    | O => m
    | S n' => S (plus n' m)
  end.

(** Para nuestra mayor comodidad podemos definir notación que hará más legibles 
    las expresiones numéricas. *)
Notation "x + y" := (plus x y)  (at level 50, left associativity) : nat_scope.

Eval simpl in (plus (S (S (S O))) (S (S O))). (* 3 + 2 = 5 *)
(** Esta es la simplificación que lleva a cabo Coq mediante la aplicación de [simpl] :
       [plus (S (S (S O))) (S (S O))]    
   ==> [S (plus (S (S O)) (S (S O)))]    segunda claúsula del [match]
   ==> [S (S (plus (S O) (S (S O))))]    segunda claúsula del [match]
   ==> [S (S (S (plus O (S (S O)))))]    segunda claúsula del [match]
   ==> [S (S (S (S (S O))))]             primera claúsula del [match]  *) 
Eval simpl in (S (S (S (S O)))) + (S (S (S O))). (* 4 + 3 = 7 *)


(** ¿Pero estamos seguros de que es correcta nuestra definición de la suma? Definamos un 
    tipo inductivo que represente la suma en forma de Prop y que especifique cómo se debe 
    comportar la suma *)
Inductive essuma: nat -> nat -> nat -> Prop :=
| essumaO: forall p:nat, essuma O p p
| essumaS: forall p q r:nat, essuma p q r -> (essuma (S p) q (S r)).

(** Comprobemos si nuestra función se corresponde con las propiedades esperadas *)
(** La táctica [elim] aplica el principio de inducción correspondiente al tipo del 
    término indicado, dividiendo el goal inicial "general" en subgoals para los 
    caso(s) base(s) y caso(s) recursivo(s). 

    En el caso de pruebas por inducción en [nat]: el caso base será O, el cero, y el
    caso recursivo el resultado de aplicar S sobre un [nat] anterior. El razonamiento 
    es el siguiente:
      - Demostrar que [P] es true cuando [m] es [nil].
      - Asumiendo que [P] es true en [n0], demostrar que también lo es
        cuando [m] es [S n0]. *)

Theorem testSuma: forall m n:nat, essuma m n (plus m n).
Proof.
  intros.
  elim m.
  simpl.
  apply essumaO.
  intro.
  simpl.
  apply essumaS.
Qed.


(**  La táctica [reflexivity] simplifica (i.e. [simpl]) ambos lados de una igualdad 
     y seguidamente comprueba si son iguales *)
Theorem plus_O_n' : forall n:nat, 0 + n = n.
Proof.
  reflexivity.  
Qed.


(** Podemos definir también la función predecesor: *)
Definition pred (n : nat) : nat :=
  match n with
    | O => O
    | S n' => n'
  end.
(** El predecesor de [O] es [O]. El predecesor de [S n'] es [n'] *)


(** La función [beq_nat] es una función booleana que determina si dos [nat] son iguales. 
    En esta definición utilizamos [match] anidados. Recuérdese también que [bool] es
    también un tipo inductivo cuyos únicos miembros son [true] y [false].
Inductive bool : Type :=
  | true : bool
  | false : bool. 
*)

Fixpoint beq_nat (n m : nat) : bool :=
  match n with
  | O => match m with
         | O => true
         | S m' => false
         end
  | S n' => match m with
            | O => false
            | S m' => beq_nat n' m'
            end
  end.

Theorem plus_1_neq_0 : forall n : nat,
  beq_nat (n + 1) 0 = false.
Proof.
  intros n. 
  elim n.
    reflexivity.
    reflexivity.  
Qed.

(** Demostraremos que no hay ningún natural que al sumarle 1 obtengamos 0 *)
(** [destruct] genera un subobjetivo (uno por constructor) en base a la definición 
    inductiva del tipo del término objetivo. Es, pues, similar a [elim], si bien [elim] 
    se basa en hacer un análisis por casos aplicando el principio de inducción del 
    tipo mientras que [destruct] hace un análisis por casos en base a los constructores 
    de dicho tipo.

    En este caso se trata de [n:nat], por lo que pasamos a considerar por separado 
    los casos [n = O] y [n = S n']

    La anotación "[as [| n']]" es opcional y le indica a Coq como debe nombrar 
    la nueva variable que aparece en el segundo subobjetivo. 
*)
Theorem plus_1_neq_0bis : forall n : nat,
  beq_nat (n + 1) 0 = false.
Proof.
  intros n. 
  destruct n as [| n'].
    reflexivity.
    reflexivity.  
Qed.





(* ###################################################### *)
(** * Listas de números *)

(** Una lista es o bien la lista vacia o bien un par formado por un número
    y una lista *)
Inductive natlist : Type :=
  | nil : natlist
  | cons : nat -> natlist -> natlist.

(** Por ejemplo, una lista de tres elementos: *)
Definition l_123 := cons 1 (cons 2 (cons 3 nil)).

(** Definimos notación que nos facilite la escritura de listas. *)
Notation "x :: l" := (cons x l) (at level 60, right associativity).
Notation "[ ]" := nil.
Notation "[ x , .. , y ]" := (cons x .. (cons y nil) ..).

(** [length] calcula la longitud de una lista. *)
Fixpoint length (l:natlist) : nat := 
  match l with
  | nil => O
  | h :: t => S (length t)
  end.

(** [app] concatena dos listas. *)
Fixpoint app (l1 l2 : natlist) : natlist := 
  match l1 with
  | nil    => l2
  | h :: t => h :: (app t l2)
  end.

(** Definimos notación para escribir [app] de forma infija. *)
Notation "x ++ y" := (app x y) 
                     (right associativity, at level 60).

Example test_app1:             [1,2,3] ++ [4,5] = [1,2,3,4,5].
Proof. reflexivity.  Qed.
Example test_app2:             nil ++ [4,5] = [4,5].
Proof. reflexivity.  Qed.
Example test_app3:             [1,2,3] ++ nil = [1,2,3].
Proof. reflexivity.  Qed.

(** [hd] devuelve el primer elemento de una lista o [default] si está vacía. *)
Definition hd (default:nat) (l:natlist) : nat :=
  match l with
  | nil => default
  | h :: t => h
  end.

(* [tail] devuelve una lista sin su primer elemento. *)
Definition tail (l:natlist) : natlist :=
  match l with
  | nil => nil  
  | h :: t => t
  end.


(** * Teoremas sobre listas *)

(** Al concatenarle a [nil] una lista obtenemos dicha lista *)
Theorem nil_app : forall l:natlist,
  [] ++ l = l.
Proof.
   reflexivity.  
Qed.

(** La longitud de la cola de una lista es la longitud de la lista menos 1 *)
Theorem tl_length_pred : forall l:natlist,
  pred (length l) = length (tail l).
Proof.
  intros l. destruct l as [| n l'].
  (* Caso "l = nil". *)
    reflexivity.
  (* Caso "l = cons n l'". *) 
    reflexivity.
Qed.



(** Pruebas por inducción en listas: el caso base será la lista vacía, el
    caso recursivo el resultado de añadir una elemento en la cabeza de una lista
    genérica. 

    El razonamiento es el siguiente:

      - Demostrar que [P] es true cuando [l] es [nil].

      - Asumiendo que [P] es true en [l'], demostrar que también lo es
        cuando [l] es [cons n l']. *)

(** [induction] combina [intros] y [elim], es decir, aplica el principio de inducción
    correspondiente al tipo del término indicado *)

(** [rewrite] reescribe el objetivo en función de la igualdad indicada. 
    La flecha en el [rewrite] indica a Coq que debe rescribir de izquierda
    a derecha: sustituir length (l1' ++ l2) por length l1' + length l2 de acuerdo
    a la hipótesis IHl1'. Para reescribir de derecha a izquierda
    escribiríamos [rewrite <-]. *)

Theorem app_length : forall l1 l2 : natlist,
  length (l1 ++ l2) = (length l1) + (length l2).
Proof.
  intros l1 l2. 
  induction l1 as [| n l1'].
  (* Caso "l1 = nil" *)
  reflexivity.
  (* Caso "l1 = cons" *)
  simpl. rewrite -> IHl1'. reflexivity.  
Qed.

