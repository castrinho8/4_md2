(* EJEMPLOS DE CÁLCULOS DE PROPOSICIONES *)

(*** Ejemplo I ***************************)

Section EjemploI. (* empleare secciones para evitar conflictos *)

Hypothesis A:Prop.
Lemma var: A->A.
Proof.
intro HX.
assumption.
Qed.
Check var.

End EjemploI.


(*** Ejemplo II **************************)

Section EjemploII.

Lemma trivial: forall p:Prop, p->p.
Proof.
(*Undo. (* no funciona en coqide, usar la flecha arriba en ese caso *)*)
intros q H.
exact H.
Qed.
Check trivial.
Print trivial.

End EjemploII.


(*** Ejemplo IIIa *************************)

Section EjemploIIIa.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
intros H H0 H1.
exact ((H H1) (H0 H1)).
Qed.

Print Unnamed_thm. 
End EjemploIIIa.

(* --------------- *)


Section EjemploIIIb.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
auto.
Save pruebaIIIb.
Print pruebaIIIb.

End EjemploIIIb.

(* --------------- *)

Section EjemploIIIc.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
intros H H0 H1.
apply H.
Show 2. (* cuando hay mas de 1 subgoal solo se muestra completamente el primero; Show me permite ver completa la i-esima subgoal *)
assumption.
apply H0.
assumption.
Save pruebaIIIc.

Print pruebaIIIc.

End EjemploIIIc.


