
(* DEFINICI�N DE LA FUNCI�N "DUPLICAR" EMPLEANDO Fixpoint *)

Fixpoint doble (n: nat): nat :=
    match n with
    | O => O
    | S p => S ( S (doble p) )
    end.

Eval compute in doble O. (* doble 0 = 0 *)
Eval compute in doble (S(S(S(S O)))). (*doble 4 = 8 *)

(* DEFINICI�N DE LA FUNCI�N "SUMAR" EMPLEANDO Fixpoint *)

Fixpoint miplus (n m:nat) {struct n} : nat :=
    match n with
    | O => m
    | S p => S (miplus p m)
    end.

Eval compute in miplus O (S(S O)). (* 0+2=2 *)
Eval compute in miplus (S O) (S(S O)). (* 1+2=3 *)


(* DEMOSTRACI�N DE SU CORRECCI�N*)

(* 1. Definimos un tipo que especifica las propiedades de la suma *)
Inductive essuma: nat->nat->nat->Prop :=
    essumaO: forall n:nat, essuma O n n
  | essumaS: forall p q r:nat, essuma p q r -> essuma (S p) q (S r).

(* 2. Definimos un teorema que los relacione *)
Theorem testsuma: forall m n:nat, (essuma m n (miplus m n)).

intros.
elim m.
simpl.
Search essuma.
apply essumaO.

intros.
simpl.
Search essuma.
apply essumaS.
assumption.


