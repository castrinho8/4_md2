(* EJEMPLOS CON MODUS PONENS *)

Section EjemplosModusPonens.


(*** Ejemplo VIIa ***********************)

Section EjemploVIIa.

Theorem modusponens_a: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
apply H0.	(* 1a opción, aplicar Apply *)
assumption.
Qed.

End EjemploVIIa.

Check modusponens_a.
Print modusponens_a.


(*** Ejemplo VIIb ***********************)

Section EjemploVIIb.

Theorem modusponens_b: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
cut A.	(* 2a opción, aplicar Cut *)
assumption.
assumption.
Qed.

End EjemploVIIb.

Check modusponens_b.
Print modusponens_b.


(*** Ejemplo VIIc ***********************)

Section EjemploVIIc.

Theorem modusponens_c: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
exact (H0 H).	(* 3a opción, aplicar Exact *)
Qed.

End EjemploVIIc.

Check modusponens_c.
Print modusponens_c.


(*** Ejemplo VIId ***********************)

Section EjemploVIId.

Theorem modusponens_d: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
auto.	(* 4a opción, aplicar Auto *)
Qed.

End EjemploVIId.

Check modusponens_d.
Print modusponens_d.



Section EjemplosModusPonens.

