Variable n: nat.
Check n.
Check O.
Check S(0).(*funcion siguiente*)
Check S.
Check gt.(*funcion mayor qué (>)*)

Definition uno := S(0).
Check uno.
Print uno.
Definition dos:nat := S (S 0).
Check dos.
Print dos.
Definition tres := S(S (S 0)):nat.
Check tres.
Print tres.

Check plus.(*SUMA*)
Definition doble1 := fun m:nat => plus m m.
Check doble1.
Print doble1.

Definition doble2 (m:nat) := plus m m.
Check doble2.
Print doble2.

Check doble2 0.
Eval compute in doble2 3.
Check (forall m:nat, gt m 0).(*Para todo m de tipo nat comparamos si m > 0*)

